package com.example.inmotion;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Adapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private Repo repo;

    private static final int PERPAGE = 8;
    private static int lastId = 1;

    static ArrayList<User> users;
    static ArrayList<User> originalusers;
    static UserAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText editText = findViewById(R.id.et_search);
        RecyclerView recyclerView = findViewById(R.id.rv_users);
        Button button = findViewById(R.id.btn_loadmore);

        repo = Repo.getInstance();
        users = new ArrayList<>();
        adapter = new UserAdapter(this,users);
        originalusers = new ArrayList<>();

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadUser();
            }
        });

//        editText.setOnEditorActionListener(
//                new TextView.OnEditorActionListener() {
//                    @Override
//                    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
//                        if(i== EditorInfo.IME_ACTION_SEARCH){
//                            String query = editText.getText().toString();
//                            ArrayList<User> filtered = new ArrayList<>();
//                            for(User user : originalusers){
//                                if(user.getLogin().contains(query)){
//                                    filtered.add(user);
//                                }
//                            }
//                            users.clear();
//                            users.addAll(filtered);
//                            adapter.notifyDataSetChanged();
//                        }
//
//                        return false;
//                    }
//                }
//        );

        editText.addTextChangedListener(
                new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        String query = editText.getText().toString();
                           ArrayList<User> filtered = new ArrayList<>();
                            for(User user : originalusers){
                                if(user.getLogin().contains(query)){
                                    filtered.add(user);
                                }
                            }
                            users.clear();
                            users.addAll(filtered);
                            adapter.notifyDataSetChanged();
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {

                    }
                }
        );

        loadUser();
    }

    private void loadUser() {
        new LoadUserTast(repo).execute(lastId);
    }

    static class LoadUserTast extends AsyncTask<Integer,Void, List<User>>{
        private Repo repo;

        public LoadUserTast(Repo repo) {
            this.repo = repo;
        }

        @Override
        protected List<User> doInBackground(Integer... integers) {
            int lastUserId = integers[0];
            try {
                Response<List<User>> response = repo.getUser(lastUserId,PERPAGE).execute();
                return response.body();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<User> users) {
            super.onPostExecute(users);

            Log.d("HHHHHHHHHHHHHHHHHHHHH", "users loaded " + users.size());

            MainActivity.lastId = users.get(PERPAGE-1).getId();
            MainActivity.users.addAll(users);
            adapter.notifyDataSetChanged();
            originalusers.addAll(users);
        }
    }

}
