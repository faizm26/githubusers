package com.example.inmotion;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface UserService {
    @GET("users")
    Call<List<User>> getUser(
            @Query("since") int since,
            @Query("per_page") int banyakUser
    );

}
