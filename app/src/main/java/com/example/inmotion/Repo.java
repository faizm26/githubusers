package com.example.inmotion;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Query;

public class Repo {
    private static Repo instance;
    private Retrofit client;
    private UserService service;

    private Repo(){
        Gson gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();

        client = new Retrofit.Builder()
                .baseUrl("https://api.github.com/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        service=client.create(UserService.class);
    }

    static Repo getInstance(){
        if(instance==null){
            instance = new Repo();
        }
        return instance;
    }

    Call<List<User>> getUser(
            int since,
            int banyakUser
    ) {
        return service.getUser(since,banyakUser);
    }

}
